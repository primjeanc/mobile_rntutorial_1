import React, { useState } from "react";
import { StyleSheet, View, Button, FlatList } from "react-native";
import GoalItem from "./components/GoalItem";
import GoalInput from "./components/GoalInput";

const Goals = () => {
  <GoalInput />;
  const [courseGoals, setCourseGoals] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  //console.log aqui eh chamado pelo render (quando sai de uma funcao)
  // dentro de um metodo teria apenas a versao antes da acao
  //CTRL + M no emulador android (+D para IOS) abre a opcao de DEBUGGER (igual web debugg)
  const addGoalHandler = newGoal => {
    if (newGoal.length === 0) {
      return;
    }
    setCourseGoals(currentGoals => [
      ...currentGoals,
      { id: Math.random().toString(), value: newGoal }
    ]);
    //setCourseGoals(currentGoals => [...currentGoals, enteredGoal]);
    console.log(courseGoals);
    setIsOpen(false);
  };

  const removeGoalHandler = goalId => {
    setCourseGoals(currentGoals => {
      return currentGoals.filter(g => g.id !== goalId);
    });
  };

  const cancelGoalModalAdd = () => {
    setIsOpen(false);
  };

  return (
    <View style={styles.container}>
      <Button title="Add New Goal" onPress={() => setIsOpen(true)} />
      <GoalInput
        visible={isOpen}
        onAddGoal={addGoalHandler}
        onCancel={cancelGoalModalAdd}
      />
      <FlatList
        keyExtractor={(item, index) => item.id} // keyExtractor serve pra objetos c/ KEY diferentes de 'key', como 'ID' ou 'COD'
        data={courseGoals}
        renderItem={itemData => (
          <GoalItem
            id={itemData.item.id}
            onDelete={removeGoalHandler}
            // onDelete={() => console.log("Stop touching me insect")}
            title={itemData.item.value}
          />
        )}
      />

      {/*  {courseGoals.map((g) => 
           <View key={g} style={styles.listItem}> 
             <Text >{g}</Text>
             </View>
          )}
         */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    backgroundColor: "#fff",
    justifyContent: "center"
  }
});
export default Goals;
