import React, { useState } from "react";
import {
  View,
  TextInput,
  Button,
  StyleSheet,
  Modal,
  ShadowPropTypesIOS
} from "react-native";

const GoalInput = props => {
  const [enteredGoal, setEnteredGoal] = useState("");

  const goalInputHandler = enteredText => {
    setEnteredGoal(enteredText);
  };

  const addGoalHandler = () => {
    props.onAddGoal(enteredGoal);
    setEnteredGoal("");
  };

  return (
    <Modal visible={props.visible} animationType="slide">
      <View style={styles.upperPart}>
        <TextInput
          placeholder="Tutorial Goals"
          style={styles.input}
          onChangeText={goalInputHandler}
          value={enteredGoal}
        />
        <View style={styles.buttonView}>
          <View style={styles.button}>
            <Button title="Cancel" color="red" onPress={props.onCancel} on />
          </View>
          <View style={styles.button}>
            <Button title="add" onPress={addGoalHandler} on />
          </View>
        </View>

        {/* <Button title="add" onPress={() => props.onAddGoal(enteredGoal)} /> */}
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  upperPart: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  input: {
    width: "80%",
    borderColor: "black",
    borderWidth: 1,
    padding: 3,
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10
  },
  buttonView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    margin: 1
  },
  button: {
    width: "39%",
    margin:5
  }
});

export default GoalInput;
