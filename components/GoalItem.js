import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

const GoalItem = props => {
  return (
     <TouchableOpacity onPress={props.onDelete.bind(this, props.id)}>
      <View style={styles.listItem}>
        <Text>{props.title}</Text>
      </View>
     </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  listItem: {
    padding: 10,
    margin: 7,
    backgroundColor: "#dddd",
    borderColor: "black",
    borderWidth: 1,
    width: "100%",
    alignSelf: "center"
  }
});

export default GoalItem;
